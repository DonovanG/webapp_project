import React, { Component } from 'react';
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    LineSeries
} from 'react-vis';
import axios from 'axios'

/**
 * display the charts of the Home view with all the analyzes and statistics of the baskets
 */
class ClientBasketEvolution extends Component {

    state = {
        client: this.props.client,
        mostPrefCurrency: this.props.client.fruit_currency_preference_1,
        leastPrefCurrency: this.props.client.fruit_currency_preference_3,
        bestData: [],
        worstData: [],
        baskets: []
    }

    componentWillMount(){

        const idClient = this.props.client.id
        axios.get(`https://goldbaum-test-project.herokuapp.com/api/baskets/?id_client=${idClient}/`)
        .then((res) => {
            console.log(res)
            this.setState({baskets: res.data})        })
        .catch((error)=>{
            console.log(error);
         });

    }

  render() {
      return(
        <div>
            <div>
            <XYPlot width={300} height={300} >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <LineSeries
                data={[{x: 1, y: 11}, {x: 1.5, y: 29}, {x: 3, y: 7}]}
            />

            <LineSeries
                data={[{x: 1, y: 20}, {x: 1.5, y: 10}, {x: 3, y: 7}]}
            />
            </XYPlot>
            </div>
     

        </div>
    );
  }
}

export default ClientBasketEvolution;