import React from 'react';
import { Select, Input, Form } from 'antd';
import axios from 'axios';
import FruitListView from '../containers/FruitListView'
import { withNamespaces } from 'react-i18next';

const Option = Select.Option;
  
  /**
   * form of the fruit search page
   */
  class CustomForm extends React.Component {

 
    /**
     * constructor that initialize the fields
     * @param {*} props 
     */
    constructor(props){

      super(props);
      this.state = {
        //list
        fruits: [],
        //filter attributes
        id: 0,
        name: '',
        pcountry: '',
        currency: 'currency',
        size: 'size',
        metric: 'metric',

      };

      this.handleChange = this.handleChange.bind(this)

    }

    /**
     * change the state values with the new field value
     * @param {*} e 
     */
    handleChange(e) {

      this.setState({ 
        [e.target.name]: e.target.value 
      });
      
    }

    /**
     * change the currency value with the one of the field
     * @param {}
     } value 
     */
    handleChangeCurrency(value) {

      this.setState({ 
        currency: value 
      });

    }

    /**
     * change the size value with the one of the field
     * @param {}
     } value 
     */
    handleChangeSize(value) {

      this.setState({ 
        size: value 
      });

    }


    /**
     * change the metric value with the one of the field
     * @param {}
     } value 
     */
    handleChangeMetric(value) {

      this.setState({ 
        metric: value 
      });

    }

    /**
     * get the fruits list from the django server
     */
    componentWillMount(){
      axios.get('https://goldbaum-test-project.herokuapp.com/api/fruit/')
      .then(res => {
          this.setState({
              fruits: res.data
          });
      })
      
    }


    render() {
      
      /** 
       * function that filter the fruit list in fuction of the inputs 
      */
      let filteredFruit = this.state.fruits.filter(
        (fruit) => {
        let fruitId = fruit.id
        
        let fruitName = fruit.name.toLowerCase()
        let pcountry = fruit.prohibited_country.toLowerCase()
        let currency = fruit.currency.toLowerCase()
        let size = fruit.size.toLowerCase()
        let metric = fruit.metric.toLowerCase()

       return ((
        (fruitId == this.state.id || this.state.id==0) &&
        (fruitName.startsWith(this.state.name.toLowerCase())) &&
       (pcountry.startsWith(this.state.pcountry.toLowerCase())) &&
       (currency.startsWith(this.state.currency.toLowerCase()) || this.state.currency.toLowerCase()==="currency") &&
       (size.startsWith(this.state.size.toLowerCase()) || this.state.size.toLowerCase()==="size") &&
       (metric.startsWith(this.state.metric.toLowerCase()) || this.state.metric.toLowerCase()==="metric"))
       );
        }
      );      

      return (
        <div>
          <Form layout="inline">
              <Form.Item>
                <Input type="number" min={1} max={1000} 
                placeholder={this.props.t('Fruit id')}
                name="id" 
                value={this.state.id}
                onChange={this.handleChange.bind(this)}
                />
              </Form.Item>

              <Form.Item>
                <Input 
                placeholder={this.props.t('Name')} 
                style={{width: 120}}  
                name="name"
                value={this.state.name}
                onChange={this.handleChange.bind(this)}
                />
              </Form.Item>

              <Form.Item>
                <Input 
                placeholder={this.props.t('pcountry')}
                style={{width: 150}}
                name="pcountry" 
                value={this.state.pcountry}
                onChange={this.handleChange.bind(this)}
                />
              </Form.Item>

              <Form.Item>
                <Select defaultValue={this.props.t('Currency')} 
                style={{ width: 120 }} 
                onChange={this.handleChangeCurrency.bind(this)}
                >
                  <Option value="euro">Euro</Option>
                  <Option value="dollard">Dollar</Option>
                  <Option value="sterling">Sterling</Option>
                </Select>
              </Form.Item>

              <Form.Item>
                <Select defaultValue={this.props.t('Size')}
                 style={{ width: 120 }}
                 onChange={this.handleChangeSize.bind(this)}
                 >
                  <Option value="small">{this.props.t('small')}</Option>
                  <Option value="medium">{this.props.t('medium')}</Option>
                  <Option value="large">{this.props.t('large')}</Option>
                </Select>
              </Form.Item>

              <Form.Item>
                <Select defaultValue={this.props.t('Metric')} 
                style={{ width: 120 }} 
                onChange={this.handleChangeMetric.bind(this)}
                >
                  <Option value="number">{this.props.t('number')}</Option>
                  <Option value="weight">{this.props.t('weight')}</Option>
                </Select>
              </Form.Item>      
        </Form>

        <FruitListView value="list" fruits={filteredFruit} limit={10} />

      </div>
      );
    }



    
  }
  
export default withNamespaces()(CustomForm);