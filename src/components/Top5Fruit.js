import React from 'react';
import { Button } from 'antd';
import axios from 'axios'
import FruitListView from '../containers/FruitListView'
import { withNamespaces } from 'react-i18next';

/**
 * display the top 5 of the fruits filtered by specific fields
 */
class TopFruit extends React.Component {

    /**
     * constructor that initialize some fields and filtered tables
     * @param {*} props 
     */
    constructor(props){
        super(props);
        this.state = {
            title: 'valeur moyenne des paniers par pays',
            baskets: [],
            filteredyCountry: [],
            filteredyGender: [],
            filteredyCity: [],
            filteredyCurrency: [],
            data: [],
            fruits: []


        };


    }

    /**
     * get the baskets and the fruits from the django server
     */
    componentWillMount(){
        axios.get('https://goldbaum-test-project.herokuapp.com/api/basket/')
        .then(res => {

            this.setState({
                baskets: res.data
            });

        })

        var baskets = this.state.baskets

        this.setState({
          filteredyCountry: baskets,
          filteredyGender: baskets,
          filteredyCity: baskets,
          filteredyCurrency: baskets

        })

        axios.get('https://goldbaum-test-project.herokuapp.com/api/fruit/')
        .then((res) => {
            console.log(res)
            this.setState({fruits: res.data})
        })
        .catch((error)=>{
            console.log(error);
         });
         
        const lenght = this.state.fruits.length
        for (var i = 0; i < lenght-5; i++) {
            this.state.fruits.pop()
          }
        
          this.setState()
        
                

      }

    /**
     * filter by coutnry
     */
    countryOnClick(){
        const baskets = this.state.baskets
        var tab = baskets.map((basket) => {
            console.log(basket.id_client)

            var items = {};
            items.x =  basket.id_client.country;
            items.y = parseFloat(basket.id_fruit.price_month1) * basket.volume; 
            return items;
        });

        console.log(tab)

    }

    /**
     * filter by gender
     */
    genderOnClick(){
        
    } 
    
    /**
     * filter by city
     */
    cityOnClick(){
        
    }  
    
    /**
     * filter by currency
     */
    currencyOnClick(){
        
    }


    /**
     * display the form with the top 5 list coresponding
     */
    render(){

        return(

            <div>
            <h2>{this.state.title}</h2>



            <div>
                <Button onClick={this.countryOnClick.bind(this)}>{this.props.t('Country')}</Button>
                <Button onClick={this.genderOnClick.bind(this)}>{this.props.t('Sexe')}</Button>
                <Button onClick={this.cityOnClick.bind(this)}>{this.props.t('City')}</Button>
                <Button onClick={this.currencyOnClick.bind(this)}>{this.props.t('Currency')}</Button>

            </div>

                <FruitListView value="list" fruits={this.state.fruits} />



            </div>

        );


    }


}

export default withNamespaces()(TopFruit);