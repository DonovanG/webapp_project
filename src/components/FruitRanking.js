import React from 'react'
import FruitList from '../containers/FruitListView'
import axios from 'axios'
class Ranking extends React.Component{


    state = {
        client: this.props.client,
        fruits: []
    }

    componentWillMount(){


        axios.get('https://goldbaum-test-project.herokuapp.com/api/fruit/')
        .then((res) => {
            this.setState({fruits: res.data})
            console.log("fruits : ", res.data)

        })
        .catch((error)=>{
            console.log(error);
         });

         


    }

    render(){
        let filteredFruits = []

        filteredFruits = this.state.fruits.filter((fruit) => {
            const fruitCountry = fruit.prohibited_country
            return(
                fruitCountry !== this.state.client.country
            );
         });

         console.log("filtered fruits : ", filteredFruits)
        return(
            <div>
                <FruitList fruits={filteredFruits} limit={3} />
            </div>
        )
    }
}

export default Ranking;