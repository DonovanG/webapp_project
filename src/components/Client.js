import React from 'react';
import { List, Avatar } from 'antd';

/** display a list of clients */
const Clients = (props) => {
    return(


        <List
            itemLayout="vertical"
            size="large"
            pagination={{
            onChange: (page) => {
                console.log(page);
            },
            pageSize: 3,
            }}
            dataSource={props.data}
            footer={<div></div>}
            renderItem={item => (
            <List.Item
                key={item.title}
            >
                <List.Item.Meta
                avatar={<Avatar src={"https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png"} />}
                title={<a href={`/client/${item.id}`}>{item.name}</a>}
                description={item.country}
                />
            </List.Item>
            )}
        />
    )
}

export default Clients;