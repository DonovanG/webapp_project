import React from 'react'
import {
    Form, DatePicker, Button,
  } from 'antd';

  const { MonthPicker } = DatePicker;
  
  /**
   * this component allow to take two date ad do an action with it
   */
  class DateFilter extends React.Component {

    constructor(props){
      super(props)
      this.handleSubmit = this.handleSubmit.bind(this)
    }


    handleSubmit = (e) => {
      e.preventDefault();
  
      this.props.form.validateFields((err, fieldsValue) => {
        if (err) {
          return;
        }
  
        // Should format date value before submit.
        this.setState({
            values: {
                ...fieldsValue,
                'month_picker': fieldsValue['month_picker'].format('MM'),
                'month_picker2': fieldsValue['month_picker2'].format('MM'),
      
              }
        }, () => this.props.handleSubmit(this.state.values));
        
      });


    }
  
    render() {
      const { getFieldDecorator } = this.props.form;
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
      };
      const config = {
        rules: [{ type: 'object', required: true, message: 'Please select month!' }],
      };

      return (
        <Form onSubmit={this.handleSubmit}>

          <Form.Item
            {...formItemLayout}
            label="Begin month"
          >
            {getFieldDecorator('month_picker', config)(
              <MonthPicker />
            )}
          </Form.Item>

          <Form.Item
            {...formItemLayout}
            label="End month"
          >
            {getFieldDecorator('month_picker2', config)(
              <MonthPicker />
            )}
          </Form.Item>

          
          <Form.Item
            wrapperCol={{
              xs: { span: 24, offset: 0 },
              sm: { span: 16, offset: 8 },
            }}
          >
            <Button type="primary" htmlType="submit">Submit</Button>
          </Form.Item>
        </Form>
      );
    }
  }
  
  
export default Form.create({ name: 'time_related_controls' })(DateFilter);