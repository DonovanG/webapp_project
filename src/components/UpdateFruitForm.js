import React from 'react';
import {
    Form, Input, Select, InputNumber, Button
  } from 'antd';
  
import axios from 'axios';
import { withNamespaces } from 'react-i18next';



  const { Option } = Select;
  
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 5 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };
  
  /**
   * this class allow to update a fruit information in the database
   */
  class UpdateFruitForm extends React.Component {

    /**
     * initialize the fields with the fruit's ones
     * @param {*} props 
     */
    constructor(props){
        super(props);
        this.state = {
            currencyValue: this.props.fruit.currency, 
            sizeValue: this.props.fruit.size, 
            metricValue: this.props.fruit.metric
        };
      }
   
    /**
     * handle the submit event from the update button and update the fruit
     */
    handleFormSubmit = async (event) => {

        const name = event.target.elements.name.value;
        const pCountry = event.target.elements.pCountry.value;
        const currency = this.state.currencyValue;
        const size = this.state.sizeValue;
        const metric = this.state.metricValue;
        const priceUnit = event.target.elements.priceUnit.value;
        const pm1 = event.target.elements.pm1.value
        const pm2 = event.target.elements.pm2.value
        const pm3 = event.target.elements.pm3.value
        const pm4 = event.target.elements.pm4.value
        const pm5 = event.target.elements.pm5.value
        const pm6 = event.target.elements.pm6.value
        const pm7 = event.target.elements.pm7.value
        const pm8 = event.target.elements.pm8.value
        const pm9 = event.target.elements.pm9.value
        const pm10 =event.target.elements.pm10.value
        const pm11= event.target.elements.pm11.value
        const pm12 = event.target.elements.pm12.value
        const vm1 = event.target.elements.vm1.value;
        const vm2 = event.target.elements.vm2.value;
        const vm3 = event.target.elements.vm3.value;
        const vm4 = event.target.elements.vm4.value;
        const vm5 = event.target.elements.vm5.value;
        const vm6 = event.target.elements.vm6.value;
        const vm7 = event.target.elements.vm7.value;
        const vm8 = event.target.elements.vm8.value;
        const vm9 = event.target.elements.vm9.value;
        const vm10 = event.target.elements.vm10.value;
        const vm11 = event.target.elements.vm11.value;
        const vm12 = event.target.elements.vm12.value;
        const path = event.target.elements.path.value;

        console.log(name, pCountry, currency,size,metric,priceUnit,
            pm1,pm2,pm3,pm4,pm5,pm6,pm7,pm8,pm9,pm10,pm11,pm12,
            vm1,vm2,vm3,vm4,vm5,vm6,vm7,vm8,vm9,vm10,vm11,vm12,
            path)
        
        console.log("id : "+this.props.fruit.id)
        try {
        const res = await axios.put(`https://goldbaum-test-project.herokuapp.com/api/fruit/${this.props.fruit.id}/`, {
          name: name,
          prohibited_country: pCountry,
          currency: currency,
          size: size,
          metric: metric,
          priceUnit: priceUnit,
          price_month1: pm1,
          price_month2: pm2,
          price_month3: pm3,
          price_month4: pm4,
          price_month5: pm5,
          price_month6: pm6,
          price_month7: pm7,
          price_month8: pm8,
          price_month9: pm9,
          price_month10: pm10,
          price_month11: pm11,
          price_month12: pm12,
          volume_month1: vm1,
          volume_month2: vm2,
          volume_month3: vm3,
          volume_month4: vm4,
          volume_month5: vm5,
          volume_month6: vm6,
          volume_month7: vm7,
          volume_month8: vm8,
          volume_month9: vm9,
          volume_month10: vm10,
          volume_month11: vm11,
          volume_month12: vm12,
          img_path: path
        });
        return console.log(res);
      }
      catch (error) {
        return console.error(error);
      }
        
    }

    render() {
   
      return (    
      <Form onSubmit={(event) => this.handleFormSubmit(event)}>
      <Form.Item
        {...formItemLayout}
        label={this.props.t('Name')}
      >
        <Input name="name" defaultValue={this.props.fruit.name} />
      </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('pcountry')}
      >
        <Input defaultValue={this.props.fruit.prohibited_country} name="pCountry" />
      </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('Currency')}
      >
            <Select defaultValue={this.props.fruit.currency} currencyValue={this.state.currencyValue}  >  
                <Option value="euro">Euro</Option>
                <Option value="dollard">Dollard</Option>
                <Option value="sterling">Sterling</Option>
            </Select> 
        </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('Size')}
      >
        <Select defaultValue={this.props.fruit.size} sizeValue={this.state.sizeValue} >       
            <Option value="small">{this.props.t('small')}</Option>
            <Option value="medium">{this.props.t('medium')}</Option>
            <Option value="large">{this.props.t('large')}</Option>
        </Select>      
    </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('Metric')}
      >
        <Select defaultValue={this.props.fruit.metric} metricValue={this.state.metricValue} >       
            <Option value="number">{this.props.t('number')}</Option>
            <Option value="weight">{this.props.t('weight')}</Option>
        </Select>      
    </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('punit')}
      >
        <InputNumber name="priceUnit" defaultValue={this.props.fruit.price_unit}/>
      </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 1"}
      >
        <Input name="pm1" defaultValue={this.props.fruit.price_month1}/>
      </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 2"}
      >
        <Input name="pm2" defaultValue={this.props.fruit.price_month2}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 3"}
      >
        <Input name="pm3" defaultValue={this.props.fruit.price_month3}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 4"}
      >
        <Input name="pm4" defaultValue={this.props.fruit.price_month4}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 5"}
      >
        <Input name="pm5" defaultValue={this.props.fruit.price_month5}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 6"}
      >
        <Input name="pm6" defaultValue={this.props.fruit.price_month6}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 7"}
      >
        <Input name="pm7" defaultValue={this.props.fruit.price_month7}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 8"}
      >
        <Input name="pm8" defaultValue={this.props.fruit.price_month8}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 9"}
      >
        <Input name="pm9" defaultValue={this.props.fruit.price_month9}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 10"}
      >
        <Input name="pm10" defaultValue={this.props.fruit.price_month10}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 11"}
      >
        <Input name="pm11" defaultValue={this.props.fruit.price_month11}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('pm')+" 12"}
      >
        <Input name="pm12" defaultValue={this.props.fruit.price_month12}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 1"}
      >
        <InputNumber name="vm1" defaultValue={this.props.fruit.volume_month1}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 2"}
      >
        <InputNumber name="vm2" defaultValue={this.props.fruit.volume_month2}/>
      </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 3"}
      >
        <InputNumber name="vm3" defaultValue={this.props.fruit.volume_month3}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 4"}
      >
        <InputNumber name="vm4" defaultValue={this.props.fruit.volume_month4}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 5"}
      >
        <InputNumber name="vm5" defaultValue={this.props.fruit.volume_month5}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 6"}
      >
        <InputNumber name="vm6" defaultValue={this.props.fruit.volume_month6}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 7"}
      >
        <InputNumber name="vm7" defaultValue={this.props.fruit.volume_month7}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 8"}
      >
        <InputNumber name="vm8" defaultValue={this.props.fruit.volume_month8}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 9"}
      >
        <InputNumber name="vm9" defaultValue={this.props.fruit.volume_month9}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 10"}
      >
        <InputNumber name="vm10" defaultValue={this.props.fruit.volume_month10}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 11"}
      >
        <InputNumber name="vm11" defaultValue={this.props.fruit.volume_month11}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('vm')+" 12"}
      >
        <InputNumber name="vm12" defaultValue={this.props.fruit.volume_month12}/>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('path')}
      >
        <Input name="path" defaultValue={this.props.fruit.img_path}/>
      </Form.Item>

      <Form.Item>
      <Button
                type="primary"
                htmlType="submit"
            >
                {this.props.t('Update')}
            </Button>
        </Form.Item>
    </Form>
  );
      }}

  export default withNamespaces()(UpdateFruitForm);