import React from 'react';
import { List, Avatar, Icon } from 'antd';



const IconText = ({ type, text }) => (
  <span>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text}
  </span>
);

/**
 * display a list of fruit
 * @param {*} props 
 */
const Fruits = (props) => {

    const limit = props.limit;

    return(
        
        <List
            itemLayout="vertical"
            size="large"
            pagination={{
            onChange: (page) => {
                console.log(page);
            },
            pageSize: limit,
            }}
            dataSource={props.data}
            footer={<div></div>}
            renderItem={item => (
            <List.Item
                key={item.title}
                actions={[
                    <IconText type="star-o" text="note" />,
                    <IconText type="star-o" text="note" />, 
                    <IconText type="star-o" text="note" /> ]}
                    extra={<img width={150}  src={window.location.origin + item.img_path} />}
                    
            >
                <List.Item.Meta
                avatar={<Avatar src={<IconText type="fruit" />} />}
                title={<a href={`/fruit/${item.id}`}>{item.name}</a>}
                />

                
            </List.Item>
            )}
        />
    )
}

export default Fruits;