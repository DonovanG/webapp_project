import React from 'react';
import DateFilter from './DateFilter';
import PriceEvolution from './PriceEvolutionChart'
import { withNamespaces } from 'react-i18next';

/**
 * view displayed by default on the website
 * A title + a form to filter by date + some charts for analyzes and statistics
 */
class Home extends React.Component {

    state = {
        dates:{}
    }

    constructor({props}){
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
      }

    handleSubmit(value){
        this.setState({dates: value});
        console.log(value.month_picker)

    }

    

    render(){


        return (
            <div>
                <h1>{this.props.t('Home title')}</h1>
                <DateFilter handleSubmit={this.handleSubmit} />
                <PriceEvolution dates={this.state.dates} />
            </div>
        );
    }

}

export default withNamespaces()(Home);