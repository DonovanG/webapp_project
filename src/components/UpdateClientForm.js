import React from 'react';
import {
    Form, Input, Select, Button
  } from 'antd';
  
import axios from 'axios';
import { withNamespaces } from 'react-i18next';

  const { Option } = Select;
  
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 10 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };
  
  /**
   * this class allow to update a client information in the database
   */
  class CustomForm extends React.Component {

    /**
     * initialize the fields with the client's ones
     * @param {*} props 
     */
    constructor(props){
      super(props);
      this.state = {
        genderValue: this.props.client.gender,
        fc1Value: this.props.client.fruit_currency_preference_1,
        fc2Value: this.props.client.fruit_currency_preference_2,
        fc3Value: this.props.client.fruit_currency_preference_3,
        fs1Value: this.props.client.fruit_size_preference_1,
        fs2Value: this.props.client.fruit_size_preference_2,
        fs3Value: this.props.client.fruit_size_preference_3,
        ft1Value: this.props.client.fruit_type_preference_1,
        fct2Value: this.props.client.fruit_type_preference_2,
      };
    }

    /**
     * handle the submit event from the update button and update the client
     */
    handleFormSubmit = async (event) => {

      const name = event.target.elements.name.value;
      const country = event.target.elements.country.value;
      const city = event.target.elements.city.value;
      const gender = this.state.genderValue;
      const fc1 = this.state.fc1Value;
      const fc2 = this.state.fc1Value;
      const fc3 = this.state.fc1Value;
      const fs1 = this.state.fc1Value;
      const fs2 = this.state.fc1Value;
      const fs3 = this.state.fc1Value;
      const ft1 = this.state.fc1Value;
      const ft2 = this.state.fc1Value;


    

      return axios.put('https://goldbaum-test-project.herokuapp.com/api/client/',{
          name: name,
          country: country,
          city: city,
          gender: gender,
          fruit_currency_preference_1: fc1,
          fruit_currency_preference_2: fc2,
          fruit_currency_preference_3: fc3,
          fruit_size_preference_1: fs1,
          fruit_size_preference_2: fs2,
          fruit_size_preference_3: fs3,
          fruit_type_preference_1: ft1,
          fruit_type_preference_2: ft2
      })
      .then(res => console.log(res))
      .catch(error => console.error(error));

  }

    /**
     * display the form
     */
    render() {
   
      return (    
      <Form onSubmit={(event) => this.handleFormSubmit(event)}>
      <Form.Item
        {...formItemLayout}
        label={this.props.t('Name')}
      >
        <Input name="name" defaultValue={this.props.client.name}/>
      </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('Country')}
      >
        <Input defaultValue={this.props.client.country} name="country" />
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        label={this.props.t('City')}
      >
        <Input defaultValue={this.props.client.city} name="city" />
      </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('Gender')}
      >
        <Select defaultValue={this.props.client.gender} genderValue={this.state.genderValue}>       
            <Option value="m">male</Option>
            <Option value="f">female</Option>
        </Select>      
    </Form.Item>
  
      <Form.Item
        {...formItemLayout}
        label={this.props.t('fc1')}
      >
        <Select defaultValue={this.props.client.fruit_currency_preference_1} fc1Value={this.state.fc1Value}>       
            <Option value="euro">Euro</Option>
            <Option value="sterling">Sterling</Option>
            <Option value="dollard">dollard</Option>
        </Select>      
    </Form.Item>

    <Form.Item
        {...formItemLayout}
        label={this.props.t('fc2')}
      >
        <Select defaultValue={this.props.client.fruit_currency_preference_2} fc2Value={this.state.fc2Value}>       
            <Option value="euro">Euro</Option>
            <Option value="sterling">Sterling</Option>
            <Option value="dollard">dollar</Option>
        </Select>      
    </Form.Item>

    <Form.Item
        {...formItemLayout}
        label={this.props.t('fc3')}
      >
        <Select defaultValue={this.props.client.fruit_currency_preference_3} fc3Value={this.state.fc3Value}>       
            <Option value="euro">Euro</Option>
            <Option value="sterling">Sterling</Option>
            <Option value="dollard">dollar</Option>
        </Select>      
    </Form.Item>

    <Form.Item
        {...formItemLayout}
        label={this.props.t('fs1')}
      >
        <Select defaultValue={this.props.client.fruit_size_preference_1} fs1Value={this.state.fs1Value}>       
            <Option value="small">small</Option>
            <Option value="medium">medium</Option>
            <Option value="large">large</Option>
        </Select>      
    </Form.Item>

    <Form.Item
        {...formItemLayout}
        label={this.props.t('fs2')}
      >
        <Select defaultValue={this.props.client.fruit_size_preference_2} fs2Value={this.state.fs2Value} >       
            <Option value="small">small</Option>
            <Option value="medium">medium</Option>
            <Option value="large">large</Option>
        </Select>      
    </Form.Item>

    <Form.Item
        {...formItemLayout}
        label={this.props.t('fs3')}
      >
        <Select defaultValue={this.props.client.fruit_size_preference_3} fs3Value={this.state.fs3Value}>       
            <Option value="small">small</Option>
            <Option value="medium">medium</Option>
            <Option value="large">large</Option>
        </Select>      
    </Form.Item>

    <Form.Item
        {...formItemLayout}
        label={this.props.t('ft1')}
      >
        <Select defaultValue={this.props.client.fruit_type_preference_1} ft1Value={this.state.ft1Value}>       
            <Option value="currency">currency</Option>
            <Option value="size">size</Option>
        </Select>      
    </Form.Item>

    <Form.Item
        {...formItemLayout}
        label={this.props.t('ft2')}
      >
        <Select defaultValue={this.props.client.fruit_type_preference_2} ft2Value={this.state.ft2Value}>       
            <Option value="currency">currency</Option>
            <Option value="size">size</Option>
        </Select>      
    </Form.Item>
  
     
      <Form.Item>
      <Button
                type="primary"
                htmlType="submit"
            >
                {this.props.t('Update')}
            </Button>
        </Form.Item>
    </Form>
  );
      }}

  export default withNamespaces()(CustomForm);