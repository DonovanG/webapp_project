import React from 'react';
import { Menu, Icon, Button } from 'antd';
import {Link} from 'react-router-dom';
import i18n from '../i18n';
import { withNamespaces } from 'react-i18next';


const SubMenu = Menu.SubMenu;

/**
 * The main menu 
 * @param {*} props 
 */
const CustomMenu = (props) => {
    
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
 }
    return(

      <div style={{ width: 256 }}>
        <Menu
          defaultSelectedKeys={['1']}
          mode="inline"
          theme="dark"
        >
          <Menu.Item key="1">
            <Link to="/">
                <Icon type="home" />
                <span>{props.t('Home')}</span>
            </Link>
          </Menu.Item>
          <SubMenu key="sub1" title={<span><Icon type="barcode" /><span>{props.t('Fruit')}</span></span>}>
                <Menu.Item key="3">            
                <Link to="/fruit"><Icon type="search" />{props.t('Search')}</Link>
                </Menu.Item>             
                <Menu.Item key="4">
                <Link to="/fruitform"><Icon type="plus" />{props.t('Add')}</Link>
                </Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" title={<span><Icon type="user" /><span>{props.t('Client')}</span></span>}>         
                <Menu.Item key="5">
                <Link to="/client"><Icon type="search" /> {props.t('Search')}</Link>
                </Menu.Item>                         
                <Menu.Item key="6">
                <Link to="/clientform"> <Icon type="plus" />{props.t('Add')}</Link>
                </Menu.Item>
          </SubMenu>
          <Menu.Item key="2">
            <Link to="/currency">
                <Icon type="line-chart" />
                <span>{props.t('Currency')}</span>
            </Link>
          </Menu.Item>
          <br/>
          <div style={{padding: "20px"}}>
              <Button onClick={() => changeLanguage('fr')}>fr</Button>
              <Button onClick={() => changeLanguage('en')}>en</Button>
          </div>
        </Menu>
      </div>
    );
  }

export default withNamespaces()(CustomMenu);