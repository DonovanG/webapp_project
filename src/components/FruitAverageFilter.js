import React from 'react';
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    LineSeries
} from 'react-vis';
import { Button } from 'antd';
import { withNamespaces } from 'react-i18next';

/**
 * A chart component that display the basket average filtered by specific fields
 */
class AverageChart extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            title: props.t('fa title'),
            data: [],
            
            
        };
    }


    countryOnClick(){

    }
    genderOnClick(){
        
    }    
    cityOnClick(){
        
    }    
    currencyOnClick(){
        
    }

    getAverageDate(value){
        const month1 = parseInt(value.month_picker, 10);
        const month2 = parseInt(value.month_picker2, 10);

        console.log("month1 : ",month1)
        console.log("month2 : ",month2)

        for (var i = month1; i <= month2; i++) {
            console.log(i)
          }
    }

    render(){

        return(

            <div>
            <h2>{this.state.title}</h2>



            <div>
                <Button onClick={this.countryOnClick.bind(this)}>{this.props.t('Country')}</Button>
                <Button onClick={this.genderOnClick.bind(this)}>{this.props.t('Sexe')}</Button>
                <Button onClick={this.cityOnClick.bind(this)}>{this.props.t('City')}</Button>
                <Button onClick={this.currencyOnClick.bind(this)}>{this.props.t('Currency')}</Button>

            </div>
            <XYPlot width={300} height={300} >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <LineSeries
                style={{
                strokeWidth: '3px'
                }}
                lineStyle={{stroke: 'red'}}
                data={[{x: 1, y: 10}, {x: 2, y: 5}, {x: 3, y: 15}]}
                
            />
            <LineSeries
                data={[{x: 1, y: 11}, {x: 1.5, y: 29}, {x: 3, y: 7}]}
            />

            <LineSeries
                data={[{x: 1, y: 20}, {x: 1.5, y: 10}, {x: 3, y: 7}]}
            />
            </XYPlot>
            </div>

        );


    }


}

export default withNamespaces()(AverageChart);