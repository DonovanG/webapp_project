import React from 'react';
import { Input } from 'antd';
import axios from 'axios';
import CLientListView from '../containers/ClientListView'
import { withNamespaces } from 'react-i18next';

  
/**
 * display a form wich ask for the client name and render the list of clients that correspond
 */
  class CustomForm extends React.Component {

 
    /**
     * constructor for initializing the client's list and the name field
     * @param {*} props 
     */
    constructor(props){

      super(props);
      this.state = {
        //list
        clients: [],
        //filter attributes
        name: '',

      };

      this.handleChange = this.handleChange.bind(this);
    }

    /**
     * update the list with the new name field
     * @param {*} event 
     */
    handleChange(event){

        this.setState({
            name: event.target.value
          });
    }

    /**
     * get the client list
     */
    componentWillMount(){
        axios.get('https://goldbaum-test-project.herokuapp.com/api/client/')
        .then(res => {
            this.setState({
                clients: res.data,
              });
        })
  
    }


    /**
     * display the form
     */
    render() {
      
      let filteredClients = this.state.clients.filter(
        (client) => {
        let clientName = client.name.toLowerCase()
       return clientName.startsWith(this.state.name.toLowerCase())
    
        }
      );
          
      return (
        <div>
              
          <Input type="text"
          placeholder={this.props.t('Name')} 
          value={this.state.name}
          onChange={this.handleChange.bind(this)} 
          style={{width: 120}}  />
          <CLientListView value="list" clients={ filteredClients }/>

      </div>
      );
    }



    
  }
  
export default withNamespaces()(CustomForm);