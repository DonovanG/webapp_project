import React from 'react';
import {
  Form, Input, Select, InputNumber, Button
} from 'antd';

import axios from 'axios';
import { withNamespaces } from 'react-i18next';


const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 5 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

/**
 * Form component that get all the fruit information in order to add it
 * to the database
 */
class CustomForm extends React.Component {

  /**
   * constructor that get the props and put default values to some fields
   * @param {*} props 
   */
  constructor(props){
    super(props);
    this.state = {currencyValue: 'euro', sizeValue: 'small', metricValue: 'weight'};
  }
  
  /** method that handle the submit event from the form and add the fruit to the db */
  handleFormSubmit = async (event) => {

      /** get elements from the fields */
      const name = event.target.elements.name.value;
      const pCountry = event.target.elements.pCountry.value;
      const currency = this.state.currencyValue;
      const size = this.state.sizeValue;
      const metric = this.state.metricValue;

      console.log(currency)
      console.log(size)
      console.log(metric)

      const priceUnit = event.target.elements.priceUnit.value;
      const pm1 = event.target.elements.pm1.value;
      const pm2 = event.target.elements.pm2.value;
      const pm3 = event.target.elements.pm3.value;
      const pm4 = event.target.elements.pm4.value;
      const pm5 = event.target.elements.pm5.value;
      const pm6 = event.target.elements.pm6.value;
      const pm7 = event.target.elements.pm7.value;
      const pm8 = event.target.elements.pm8.value;
      const pm9 = event.target.elements.pm9.value;
      const pm10 = event.target.elements.pm10.value;
      const pm11= event.target.elements.pm11.value;
      const pm12 = event.target.elements.pm12.value;
      const vm1 = event.target.elements.vm1.value;
      const vm2 = event.target.elements.vm2.value;
      const vm3 = event.target.elements.vm3.value;
      const vm4 = event.target.elements.vm4.value;
      const vm5 = event.target.elements.vm5.value;
      const vm6 = event.target.elements.vm6.value;
      const vm7 = event.target.elements.vm7.value;
      const vm8 = event.target.elements.vm8.value;
      const vm9 = event.target.elements.vm9.value;
      const vm10 = event.target.elements.vm10.value;
      const vm11 = event.target.elements.vm11.value;
      const vm12 = event.target.elements.vm12.value;

      /** use axios to make post request in order to add the fruit */
      try {
      const res = await axios.post('https://goldbaum-test-project.herokuapp.com/api/fruit/', {
        name: name,
        prohibited_country: pCountry,
        currency: currency,
        size: size,
        metric: metric,
        priceUnit: priceUnit,
        price_month1: pm1,
        price_month2: pm2,
        price_month3: pm3,
        price_month4: pm4,
        price_month5: pm5,
        price_month6: pm6,
        price_month7: pm7,
        price_month8: pm8,
        price_month9: pm9,
        price_month10: pm10,
        price_month11: pm11,
        price_month12: pm12,
        volume_month1: vm1,
        volume_month2: vm2,
        volume_month3: vm3,
        volume_month4: vm4,
        volume_month5: vm5,
        volume_month6: vm6,
        volume_month7: vm7,
        volume_month8: vm8,
        volume_month9: vm9,
        volume_month10: vm10,
        volume_month11: vm11,
        volume_month12: vm12
      });
      return console.log(res);
    }
    catch (error) {
      return console.error(error);
    }
      
  }

  /** display the form */
  render() {
  
    return (    
    <Form onSubmit={(event) => this.handleFormSubmit(event)}>
    <Form.Item
      {...formItemLayout}
      label={this.props.t('Name')}
    >
      <Input name="name" placeholder={this.props.t('Name')} />
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pcountry')}
    >
      <Input placeholder={this.props.t('pcountry')} name="pCountry" />
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('Currency')}
    >
          <Select defaultValue="euro" currencyValue={this.state.currencyValue} >  
              <Option value="euro">Euro</Option>
              <Option value="dollard">Dollard</Option>
              <Option value="sterling">Sterling</Option>
          </Select> 
      </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('Size')}
    >
      <Select defaultValue="small" sizeValue={this.state.sizeValue}>       
          <Option value="small">small</Option>
          <Option value="medium">medium</Option>
          <Option value="large">large</Option>
      </Select>      
  </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('Metric')}
    >
      <Select defaultValue="number" metricValue={this.state.metricValue} >       
          <Option value="number">number</Option>
          <Option value="weight">weight</Option>
      </Select>      
  </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('punit')}
    >
      <InputNumber name="priceUnit" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 1 : "}
    >
      <InputNumber name="pm1" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 2 : "}
    >
      <InputNumber name="pm2" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 3 : "}
    >
      <InputNumber name="pm3" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 4 : "}
    >
      <InputNumber name="pm4" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 5 : "}
    >
      <InputNumber name="pm5" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 6 : "}
    >
      <InputNumber name="pm6" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 7 : "}
    >
      <InputNumber name="pm7" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 8 : "}
    >
      <InputNumber name="pm8" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 9 : "}
    >
      <InputNumber name="pm9" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 10 : "}
    >
      <InputNumber name="pm10" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 11 : "}
    >
      <InputNumber name="pm11" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('pm')+" 12 : "}
    >
      <InputNumber name="pm12" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 1 : "}
    >
      <InputNumber name="vm1" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 2 : "}
    >
      <InputNumber name="vm2" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 3 : "}
    >
      <InputNumber name="vm3" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 4 : "}
    >
      <InputNumber name="vm4" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 5 : "}
    >
      <InputNumber name="vm5" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 6 : "}
    >
      <InputNumber name="vm6" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 7 : "}
    >
      <InputNumber name="vm7" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 8 : "}
    >
      <InputNumber name="vm8" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 9 : "}
    >
      <InputNumber name="vm9" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 10 : "}
    >
      <InputNumber name="vm10" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 11 : "}
    >
      <InputNumber name="vm11" defaultValue="1"/>
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('vm')+" 12 : "}
    >
      <InputNumber name="vm12" defaultValue="1"/>
    </Form.Item>
    <Form.Item>
    <Button
              type="primary"
              htmlType="submit"
          >
              {this.props.t('Add')}
          </Button>
      </Form.Item>
  </Form>
);
    }}

export default withNamespaces()(CustomForm);