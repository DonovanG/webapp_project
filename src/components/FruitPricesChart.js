import React from 'react'
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    LineSeries,
    MarkSeries
} from 'react-vis';
import axios from 'axios'
import { withNamespaces } from 'react-i18next';


/**
 * Chart that display the price evolution of one fruit in each currencies
 * each line of the chart represent a currency
 */
class FruitPricesChart extends React.Component{

    /**
     * define the prices data from the fruit
     */
    state = {
        pm1: parseFloat(this.props.fruit.price_month1),
        pm2: parseFloat(this.props.fruit.price_month2),
        pm3: parseFloat(this.props.fruit.price_month3),
        pm4: parseFloat(this.props.fruit.price_month4),
        pm5: parseFloat(this.props.fruit.price_month5),
        pm6: parseFloat(this.props.fruit.price_month6),
        pm7: parseFloat(this.props.fruit.price_month7),
        pm8: parseFloat(this.props.fruit.price_month8),
        pm9: parseFloat(this.props.fruit.price_month9),
        pm10: parseFloat(this.props.fruit.price_month10),
        pm11: parseFloat(this.props.fruit.price_month11),
        pm12: parseFloat(this.props.fruit.price_month12),  
        currencies: [],
        currency: {},

        
    }


    /** format the data in order to have each currency evolution */
    componentWillMount() {
        const currencyName = (this.props.fruit.currency)
        console.log("currency name :"+currencyName)
        axios.get(`https://goldbaum-test-project.herokuapp.com/api/currency/?currency=price_${currencyName}/`)
        .then((res) => {
                this.setState(
                    {currencies: res.data}
                );
        })
        .catch( (error) => {
            console.log(error);
        });

    }


    render(){

        const currency = this.state.currencies[0]

        console.log("currency : ",currency)


        const euro = [{x:1, y:1}]
        const sterling = [{x:1, y:1}]
        const dollar = [{x:1, y:1}]

        // /** format datas to display it in the chart
        // * each month the price will be : price_month_x = price_month_x * currency.euro_month_x
        // */
        // let euro = [
        //     {x:1, y:((this.state.pm1)*(parseFloat(currency.euro_month_1)))},
        //     {x:2, y:((this.state.pm2)*(parseFloat(currency.euro_month_2)))},
        //     {x:3, y:((this.state.pm3)*(parseFloat(currency.euro_month_3)))},
        //     {x:4, y:((this.state.pm4)*(parseFloat(currency.euro_month_4)))},
        //     {x:5, y:((this.state.pm5)*(parseFloat(currency.euro_month_5)))},
        //     {x:6, y:((this.state.pm6)*(parseFloat(currency.euro_month_6)))},
        //     {x:7, y:((this.state.pm7)*(parseFloat(currency.euro_month_7)))},
        //     {x:8, y:((this.state.pm8)*(parseFloat(currency.euro_month_8)))},
        //     {x:9, y:((this.state.pm9)*(parseFloat(currency.euro_month_9)))},
        //     {x:10, y:((this.state.pm10)*(parseFloat(currency.euro_month_10)))},
        //     {x:11, y:((this.state.pm11)*(parseFloat(currency.euro_month_11)))},
        //     {x:12, y:((this.state.pm12)*(parseFloat(currency.euro_month_12)))}

        // ]

        // /** format datas to display it in the chart
        // * each month the price will be : price_month_x = price_month_x * currency.sterling_month_x
        // */
        // let sterling = [
        //     {x:1, y:((this.state.pm1)*(parseFloat(currency.sterling_month_1)))},
        //     {x:2, y:((this.state.pm2)*(parseFloat(currency.sterling_month_2)))},
        //     {x:3, y:((this.state.pm3)*(parseFloat(currency.sterling_month_3)))},
        //     {x:4, y:((this.state.pm4)*(parseFloat(currency.sterling_month_4)))},
        //     {x:5, y:((this.state.pm5)*(parseFloat(currency.sterling_month_5)))},
        //     {x:6, y:((this.state.pm6)*(parseFloat(currency.sterling_month_6)))},
        //     {x:7, y:((this.state.pm7)*(parseFloat(currency.sterling_month_7)))},
        //     {x:8, y:((this.state.pm8)*(parseFloat(currency.sterling_month_8)))},
        //     {x:9, y:((this.state.pm9)*(parseFloat(currency.sterling_month_9)))},
        //     {x:10, y:((this.state.pm10)*(parseFloat(currency.sterling_month_10)))},
        //     {x:11, y:((this.state.pm11)*(parseFloat(currency.sterling_month_11)))},
        //     {x:12, y:((this.state.pm12)*(parseFloat(currency.sterling_month_12)))}

        // ];

        // /** format datas to display it in the chart
        // * each month the price will be : price_month_x = price_month_x * currency.dollar_month_x
        // */
        // let dollar = [
        //     {x:1, y:((this.state.pm1)*(parseFloat(currency.dollard_month_1)))},
        //     {x:2, y:((this.state.pm2)*(parseFloat(currency.dollard_month_2)))},
        //     {x:3, y:((this.state.pm3)*(parseFloat(currency.dollard_month_3)))},
        //     {x:4, y:((this.state.pm4)*(parseFloat(currency.dollard_month_4)))},
        //     {x:5, y:((this.state.pm5)*(parseFloat(currency.dollard_month_5)))},
        //     {x:6, y:((this.state.pm6)*(parseFloat(currency.dollard_month_6)))},
        //     {x:7, y:((this.state.pm7)*(parseFloat(currency.dollard_month_7)))},
        //     {x:8, y:((this.state.pm8)*(parseFloat(currency.dollard_month_8)))},
        //     {x:9, y:((this.state.pm9)*(parseFloat(currency.dollard_month_9)))},
        //     {x:10, y:((this.state.pm10)*(parseFloat(currency.dollard_month_10)))},
        //     {x:11, y:((this.state.pm11)*(parseFloat(currency.dollard_month_11)))},
        //     {x:12, y:((this.state.pm12)*(parseFloat(currency.dollard_month_12)))}

        // ];


       
        return(
            <div>
                    <h2>{this.props.t('fruit prices chart title')}</h2>
                        <XYPlot width={600} height={300} >
                        <VerticalGridLines />
                        <HorizontalGridLines />
                        <XAxis title="months" />
                        <YAxis title="prices"/>
                        <LineSeries
                            style={{
                            strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'red'}}
                            curve={'curveMonotoneX'}
                            scale={'ordinal'}
                            data={euro}
                            
                        />
                        <MarkSeries data={euro} />

                        <LineSeries
                            style={{
                            strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'red'}}
                            curve={'curveMonotoneX'}
                            data={sterling}
                            
                        />
                        <MarkSeries data={sterling} />

                        <LineSeries
                            style={{
                            strokeWidth: '3px'
                            }}
                            lineStyle={{stroke: 'red'}}
                            curve={'curveMonotoneX'}
                            data={dollar}
                            
                        />
                        <MarkSeries data={dollar} />

                        </XYPlot>
                </div>
        );
    }

}

export default withNamespaces()(FruitPricesChart);


