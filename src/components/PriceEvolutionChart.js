import React, { Component } from 'react';
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    LineSeries
} from 'react-vis';
import { Button } from 'antd';

import './PriceEvolution.scss'
import BasketAverageChart from './BasketAverageFilter'
import FruitAverageChat from './FruitAverageFilter'
import TopFruit from './Top5Fruit'
import { withNamespaces } from 'react-i18next';

/**
 * display the charts of the Home view with all the analyzes and statistics of the baskets
 */
class App extends Component {

    /** initialize the chart of average */
    constructor(props){
        super(props);
        this.state = {
            averagechart: <BasketAverageChart />

        };
    }

    /** display the chart of basket average */
    basketChart(){
        this.setState({
            averagechart: <BasketAverageChart /> 
        })

    }

    /**
     * dispaly the chart of fruit average
     * @param {*} e 
     */
    fruitChart(e){
        this.setState({
            averagechart: <FruitAverageChat /> 
        })

    }


    

  render() {
      return(
        <div>
            <div>
            <h2>{this.props.t('monthly basket')}</h2>
            <XYPlot width={300} height={300} >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <LineSeries
                style={{
                strokeWidth: '3px'
                }}
                lineStyle={{stroke: 'red'}}
                data={[{x: 1, y: 10}, {x: 2, y: 5}, {x: 3, y: 15}]}
                
            />
            <LineSeries
                data={[{x: 1, y: 11}, {x: 1.5, y: 29}, {x: 3, y: 7}]}
            />

            <LineSeries
                data={[{x: 1, y: 20}, {x: 1.5, y: 10}, {x: 3, y: 7}]}
            />
            </XYPlot>
            </div>

            <div>
            <h2>{this.props.t('bought average')}</h2>
            <XYPlot width={300} height={300} >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <LineSeries
                style={{
                strokeWidth: '3px'
                }}
                lineStyle={{stroke: 'red'}}
                data={[{x: 1, y: 10}, {x: 2, y: 5}, {x: 3, y: 15}]}
                
            />
            <LineSeries
                data={[{x: 1, y: 11}, {x: 1.5, y: 29}, {x: 3, y: 7}]}
            />

            <LineSeries
                data={[{x: 1, y: 20}, {x: 1.5, y: 10}, {x: 3, y: 7}]}
            />
            </XYPlot>
            </div>

            <div>
                <Button onClick={this.basketChart.bind(this)}>{this.props.t('Basket')}</Button>
                <Button onClick={this.fruitChart.bind(this)}>{this.props.t('Fruit')}</Button>

            </div>
            
            <div>{this.state.averagechart}</div>

            <div><TopFruit /></div>

            

        </div>
    );
  }
}

export default withNamespaces()(App);