import React from 'react';
import {
    Form, Input, Select, Button
  } from 'antd';
  
import axios from 'axios';
import { withNamespaces } from 'react-i18next';

const { Option } = Select;
  
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

/**
 * this form is use to get all the client's information that we need
 * to add him to the database
 */
class CustomForm extends React.Component {

  /**
   * constructor that get props info and put default vlue to some fields
   * @param {*} props 
   */
  constructor(props){
    super(props);
    this.state = {
      genderValue: 'male',
      fc1Value: 'euro',
      fc2Value: 'euro',
      fc3Value: 'euro',
      fs1Value: 'small',
      fs2Value: 'small',
      fs3Value: 'small',
      ft1Value: 'currency',
      fct2Value: 'currency'
    };
  }

  /**
   * method that handle the submit event from the form
   * Add the new client with the good information
   */
  handleFormSubmit = (event) => {

    const name = event.target.elements.name.value;
    const country = event.target.elements.country.value;
    const city = event.target.elements.city.value;
    const gender = this.state.genderValue;
    const fc1 = this.state.fc1Value;
    const fc2 = this.state.fc1Value;
    const fc3 = this.state.fc1Value;
    const fs1 = this.state.fc1Value;
    const fs2 = this.state.fc1Value;
    const fs3 = this.state.fc1Value;
    const ft1 = this.state.fc1Value;
    const ft2 = this.state.fc1Value;

    //axios allow to use a post request to add the client to the database
    return axios.post('https://goldbaum-test-project.herokuapp.com/api/client/',{
        name: name,
        country: country,
        city: city,
        gender: gender,
        fruit_currency_preference_1: fc1,
        fruit_currency_preference_2: fc2,
        fruit_currency_preference_3: fc3,
        fruit_size_preference_1: fs1,
        fruit_size_preference_2: fs2,
        fruit_size_preference_3: fs3,
        fruit_type_preference_1: ft1,
        fruit_type_preference_2: ft2
    })
    .then(res => console.log(res))
    .catch(error => console.error(error));

}

  /** display the form */
  render() {
  
    return (    
    <Form onSubmit={(event) => this.handleFormSubmit(event)}>
    <Form.Item
      {...formItemLayout}
      label={this.props.t('Name')}
    >
      <Input placeholder={this.props.t('Name')} id="name" />
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('Country')}
    >
      <Input placeholder={this.props.t('Country')} id="country" />
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('City')}
    >
      <Input placeholder={this.props.t('City')} id="city" />
    </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('Gender')}
    >
      <Select defaultValue="male" genderValue={this.state.genderValue}>       
          <Option value="m">male</Option>
          <Option value="f">female</Option>
      </Select>      
  </Form.Item>

    <Form.Item
      {...formItemLayout}
      label={this.props.t('fc1')}
    >
      <Select defaultValue="euro" fc1Value={this.state.fc1Value}>       
          <Option value="euro">Euro</Option>
          <Option value="sterling">Sterling</Option>
          <Option value="dollard">dollard</Option>
      </Select>      
  </Form.Item>

  <Form.Item
      {...formItemLayout}
      label={this.props.t('fc2')}
    >
      <Select defaultValue="euro" fc2Value={this.state.fc2Value}>       
          <Option value="euro">Euro</Option>
          <Option value="sterling">Sterling</Option>
          <Option value="dollard">dollar</Option>
      </Select>      
  </Form.Item>

  <Form.Item
      {...formItemLayout}
      label={this.props.t('fc3')}
    >
      <Select defaultValue="euro" fc3Value={this.state.fc3Value}>       
          <Option value="euro">Euro</Option>
          <Option value="sterling">Sterling</Option>
          <Option value="dollard">dollar</Option>
      </Select>      
  </Form.Item>

  <Form.Item
      {...formItemLayout}
      label={this.props.t('fs1')}
    >
      <Select defaultValue="small" fs1Value={this.state.fs1Value}>       
          <Option value="small">small</Option>
          <Option value="medium">medium</Option>
          <Option value="large">large</Option>
      </Select>      
  </Form.Item>

  <Form.Item
      {...formItemLayout}
      label={this.props.t('fs2')}
    >
      <Select defaultValue="small" fs2Value={this.state.fs2Value} >       
          <Option value="small">small</Option>
          <Option value="medium">medium</Option>
          <Option value="large">large</Option>
      </Select>      
  </Form.Item>

  <Form.Item
      {...formItemLayout}
      label={this.props.t('fs3')}
    >
      <Select defaultValue="small" fs3Value={this.state.fs3Value}>       
          <Option value="small">small</Option>
          <Option value="medium">medium</Option>
          <Option value="large">large</Option>
      </Select>      
  </Form.Item>

  <Form.Item
      {...formItemLayout}
      label={this.props.t('ft1')}
    >
      <Select defaultValue="currency" ft1Value={this.state.ft1Value}>       
          <Option value="currency">currency</Option>
          <Option value="size">size</Option>
      </Select>      
  </Form.Item>

  <Form.Item
      {...formItemLayout}
      label={this.props.t('ft2')}
    >
      <Select defaultValue="currency" ft2Value={this.state.ft2Value}>       
          <Option value="currency">currency</Option>
          <Option value="size">size</Option>
      </Select>      
  </Form.Item>

    
    <Form.Item>
    <Button
              type="primary"
              htmlType="submit"
          >
              {this.props.t('Add')}
          </Button>
      </Form.Item>
  </Form>
);
    }}

export default withNamespaces()(CustomForm);