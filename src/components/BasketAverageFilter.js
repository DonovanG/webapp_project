import React from 'react';
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    LineSeries
} from 'react-vis';
import { Button } from 'antd';
import axios from 'axios'
import { withNamespaces } from 'react-i18next';


/** component that display the chart of the basket average
 * filter the average in function of the country, the gener, the city and the currency
 */
class AverageChart extends React.Component {

    /**
     * constructor for initializing filtered data
     * @param {*} props 
     */
    constructor(props){
        super(props);
        this.state = {
            title: props.t('ba title'),
            baskets: [],
            filteredyCountry: [],
            filteredyGender: [],
            filteredyCity: [],
            filteredyCurrency: [],
            data: [],


        };


    }

    /**
     * get the baskets from django server
     */
    componentWillMount(){
        axios.get('https://goldbaum-test-project.herokuapp.com/api/basket/')
        .then(res => {

            this.setState({
                baskets: res.data
            });

        })

        var baskets = this.state.baskets

        this.setState({
          filteredyCountry: baskets,
          filteredyGender: baskets,
          filteredyCity: baskets,
          filteredyCurrency: baskets

        })

        console.log(this.state.baskets)

        

      }

    /**
     * filter baskets by country
     */
    countryOnClick(){
        const baskets = this.state.baskets
        var tab = baskets.map((basket) => {
            console.log(basket.id_client)

            var items = {};
            items.x =  basket.id_client.country;
            items.y = parseFloat(basket.id_fruit.price_month1) * basket.volume; 
            return items;
        });

        console.log(tab)

    }

    /**
     * filter baskets by gender
     */
    genderOnClick(){
        
    }    

    /**
     * filter baskets by city
     */
    cityOnClick(){
        
    }  
    
    /**
     * filter baskets by currency
     */
    currencyOnClick(){
        
    }

    getAverageDate(value){
        const month1 = parseInt(value.month_picker, 10);
        const month2 = parseInt(value.month_picker2, 10);

        console.log("month1 : ",month1)
        console.log("month2 : ",month2)

        for (var i = month1; i <= month2; i++) {
            console.log(i)
          }
    }

    /**
     * display the component : a chart and four button to filter
     */
    render(){

        return(

            <div>
            <h2>{this.state.title}</h2>



            <div>
                <Button onClick={this.countryOnClick.bind(this)}>{this.props.t('Country')}</Button>
                <Button onClick={this.genderOnClick.bind(this)}>{this.props.t('Sexe')}</Button>
                <Button onClick={this.cityOnClick.bind(this)}>{this.props.t('City')}</Button>
                <Button onClick={this.currencyOnClick.bind(this)}>{this.props.t('Currency')}</Button>

            </div>
            <XYPlot width={300} height={300} >
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <LineSeries
                style={{
                strokeWidth: '3px'
                }}
                lineStyle={{stroke: 'red'}}
                data={[{x: 1, y: 10}, {x: 2, y: 5}, {x: 3, y: 15}]}
                
            />
            <LineSeries
                data={[{x: 1, y: 11}, {x: 1.5, y: 29}, {x: 3, y: 7}]}
            />

            <LineSeries
                data={[{x: 1, y: 20}, {x: 1.5, y: 10}, {x: 3, y: 7}]}
            />
            </XYPlot>
            </div>

        );


    }


}

export default withNamespaces()(AverageChart);