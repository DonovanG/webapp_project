import React from 'react';
import { Layout } from 'antd';
import CustomMenu from "../components/Menu";

const { Header, Content, Footer, Sider, } = Layout;

/**
 * The main app layout
 * @param {*} props 
 */
const CustomLayout = (props) => {
    return (
        <Layout>
        <Sider style={{
          overflow: 'auto', height: '100vh', position: 'fixed', left: 0,
        }}
        >
          <div className="logo" />
          < CustomMenu />
        </Sider>
        <Layout style={{ marginLeft: 200 }}>
          <Header style={{ background: '#fff', padding: 0 }}></Header>
          <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
            <div style={{ padding: 24, background: '#fff', textAlign: 'center' }}>
              {props.children}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
          </Footer>
        </Layout>
      </Layout>
    );
}

export default CustomLayout;