import React from 'react'
import axios from 'axios';
import {Card} from 'antd';

import ClientListView from './ClientListView'
import UpdateFruitForm from '../components/UpdateFruitForm'
import FruitPricesChart from '../components/FruitPricesChart'
import { withNamespaces } from 'react-i18next';

const tabList = [{
    key: 'info',
    tab: 'info',
  }, {
    key: 'edit',
    tab: 'edit',
  }, {
    key: 'clients',
    tab: 'clients',
  }, {
    key: 'prices',
    tab: 'prices',
  }];
  
/**
 * This view is displayed by clicking on a fruit in a fruitListView
 * this allow to see some information but also edit it, 
 * show some prices evolution and
 * clients who can choose the fruit
 */
class FruitDetail extends React.Component {

    state = {
        fruit: {},
        clients: [],
        key: 'info'
    }

    /** cange the tab key */
    onTabChange = (key, type) => {
        this.setState({ [type]: key });
      }

    //change the state of the objet by getting the fruit information
    componentDidMount( ){
        const fruitID = this.props.match.params.fruitID;

        axios.get(`https://goldbaum-test-project.herokuapp.com/api/fruit/${fruitID}/`)
        .then(res => {
            this.setState({
                fruit: res.data
            });
        })

        /** get all the clients  */
        axios.get('https://goldbaum-test-project.herokuapp.com/api/client/')
        .then(res => {
            /** change the clients list */
            this.setState({
                clients: res.data
            });
            console.log("clients : ", res.data)
        })
 
        
    }

    render(){

        let filteredClients = []

        /** filter the clients
         * if the client coutry equals the country where the fruit is prohibited we don't return it
         */
        filteredClients = this.state.clients.filter(
            (client) => {
                let country = client.country
        
               return (
                   country !== this.state.fruit.prohibited_country
               );
            }
        );  

        console.log("filtered clients :", filteredClients)
        
        const contentList = {
            info: 
                <div>
                    <h2>{this.props.t('Information')}</h2>
                    <p>{this.props.t('Name')} : {this.state.fruit.name}</p>
                    <p>{this.props.t('pcountry')} : { this.state.fruit.prohibited_country}</p>
                    <p>{this.props.t('Currency')} : {this.state.fruit.currency}</p>
                    <p>{this.props.t('Size')} : {this.state.fruit.size}</p>
                    <p>{this.props.t('Metric')} : {this.state.fruit.metric}</p>
                    <p>{this.props.t('punit')} : {this.state.fruit.price_unit}</p> 
                </div>             
            ,
            edit:
                <div>
                    <UpdateFruitForm fruit={this.state.fruit} />
                </div>
            ,
            clients: 
                <div>
                    <h2>{this.props.t('Client who can choose it')}</h2>
                    <ClientListView clients={filteredClients} />
                </div>,
            prices:
                <FruitPricesChart fruit={this.state.fruit}  />
          };

        

        return (
            <div>
                <Card 
                    style={{ width: '100%' }}
                    title={this.state.fruit.name}
                    tabList={tabList}
                    activeTabKey={this.state.key}
                    onTabChange={(key) => { this.onTabChange(key, 'key'); }}
                >  
                
                {contentList[this.state.key]}

                </Card>

            </div>
        )
    }
}

export default withNamespaces()(FruitDetail);