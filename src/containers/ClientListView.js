import React from 'react'

import Clients from '../components/Client';

/** display the client's list */
class ClientList extends React.Component {


    render(){
        return (
            <div>
            <Clients data={this.props.clients} />
            </div>
        )
    }
}

export default ClientList;