import React from 'react'
import Ranking from '../components/FruitRanking'
import ClientBasketEvolution from '../components/ClientBasketEvolution'
import { withNamespaces } from 'react-i18next';

/**
 * show the client's report on the detail view
 * ranking of ruits + basket average +
 */
class Report extends React.Component{

    /** set the client */
    state = {
        client: this.props.client
    }

    render(){
        return(
            <div>
                <h2>{this.props.client.name} {this.props.t('is allow to choose : ')}</h2>
                <Ranking client={this.props.client} />
                <h2>{this.props.t('Basket average evolution')}</h2>
                <ClientBasketEvolution client={this.props.client} /> 

                {/* show the price evolution of the 2 prefered fruit of the client
                <FruitPricesChart fruit={} />
                <FruitPricesChart fruit={} /> */}




            </div>
        )
    }
}

export default withNamespaces()(Report);