import React from 'react'
import axios from 'axios';

import {Card} from 'antd';
import UpdateClientForm from '../components/UpdateClientForm'
import Report from './ClientReport'
import { withNamespaces } from 'react-i18next';

const tabList = [{
    key: 'info',
    tab: 'info',
  }, {
    key: 'edit',
    tab: 'edit',
  }, {
    key: 'report',
    tab: 'report',
  }];

/**
 * this view is displayed when we click on a client in a client list view
 * it show some details on the client but also allow to update it, on show a report
 */
class ClientDetail extends React.Component {

    state = {
        client: {},
        key: 'info'
    }

    /** set the knew key */
    onTabChange = (key, type) => {
        this.setState({ [type]: key });
      }

    //change the state of the objct
    componentDidMount( ){
        const clientID = this.props.match.params.clientID;
        axios.get(`https://goldbaum-test-project.herokuapp.com/api/client/${clientID}/`)
        .then(res => {
            this.setState({
                client: res.data
            });

        })
    }

    render(){

        /** contain the differnet views that will be displayed by clicking on a specific button */
        const contentList = {
            info: 
                <div>
                    <h2>{this.props.t('Information')}</h2>
                    <p>{this.props.t('Name')} : {this.state.client.name}</p>
                    <p>{this.props.t('Country')} : { this.state.client.country}</p>
                    <p>{this.props.t('City')} : {this.state.client.city}</p>
                    <p>{this.props.t('Gender')} : {this.state.client.gender}</p>
                    <h3>{this.props.t('fc')}</h3>
                    <p>1 : {this.state.client.fruit_currency_preference_1}</p>
                    <p>2 : {this.state.client.fruit_currency_preference_2}</p>
                    <p>3 : {this.state.client.fruit_currency_preference_3}</p>
                    <h3>{this.props.t('fs')}</h3>
                    <p>1 : {this.state.client.fruit_size_preference_1}</p>
                    <p>2 : {this.state.client.fruit_size_preference_2}</p>
                    <p>3 : {this.state.client.fruit_size_preference_3}</p>
                    <h3>{this.props.t('ft')}</h3>
                    <p>1 : {this.state.client.fruit_type_preference_1}</p>
                    <p>2 : {this.state.client.fruit_type_preference_2}</p>
                </div>             
            ,
            edit:
                <div>
                    <UpdateClientForm client={this.state.client} />
                </div>
            ,
            report: 
                <div>
                    <Report client={this.state.client} />   
                </div>
          };

        /** return a card with buttons that allow to change the card view */
        return (
            <div>
                <Card 
                    style={{ width: '100%' }}
                    title={this.state.client.name}
                    tabList={tabList}
                    activeTabKey={this.state.key}
                    onTabChange={(key) => { this.onTabChange(key, 'key'); }}
                >  
                
                {contentList[this.state.key]}

                </Card>

            </div>
        )
    }
}

export default withNamespaces()(ClientDetail);