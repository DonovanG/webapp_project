import React from 'react'

import Fruits from '../components/Fruit';

/**
 * display a list of fruit
 */
class FruitList extends React.Component {

    render(){
        return (
            <div>
            <Fruits data={this.props.fruits} limit={this.props.limit}/>
            </div>
        )
    }
}

export default FruitList;