import React from 'react';
import { Route } from 'react-router-dom';

import FruitDetail from './containers/FruitDetailView';
import ClientForm from './components/ClientForm';
import ClientDetail from './containers/ClientDetailView';
import FruitForm from './components/FruitForm';
import FruitCustomForm from './components/AddFruitForm';
import ClientCustomForm from './components/AddClientForm'
import PriceEvolutionChart from './components/PriceEvolutionChart'
import Home from './components/Home';

/**  evaluate what has to be displayed in the App component */
const BaseRouter = () => (
    <div>
        <Route exact path='/' component={Home} />
        <Route exact path='/fruit/' component={FruitForm} />
        <Route exact path='/fruit/:fruitID/' component={FruitDetail} />
        <Route exact path='/fruitform' component={FruitCustomForm} />
        <Route exact path='/client/' component={ClientForm} />
        <Route exact path='/client/:clientID/' component={ClientDetail} />
        <Route exact path='/clientform' component={ClientCustomForm} />
        <Route exact path='/currency' component={PriceEvolutionChart} />
    </div>
);

export default BaseRouter;