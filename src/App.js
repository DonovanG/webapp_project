import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import BaseRouter from './route';
import 'antd/dist/antd.css'; 

import CustomLayout from './containers/LayoutV2';


/**
 * The main application
 * the router allow to get the right view by clicking on differnent menu item
 */
class App extends Component {
  render() {
    return (
      <div className="App">
      <Router>
        <CustomLayout>
          <BaseRouter />
        </CustomLayout>
        </Router>
      </div>
    );
  }
}

export default App;
