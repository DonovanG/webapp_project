from django.contrib import admin
from django.urls import path, include, re_path
from django.views.generic import TemplateView

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    #url for the admin to add, delete, update raws in the database
    path('admin/', admin.site.urls),
    #urls used to display elements in react app
    path('api/', include('soft.api.urls')),
    #let the django server manage everything
    re_path('.*', TemplateView.as_view(template_name='index.html'))
]
