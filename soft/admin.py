from django.contrib import admin

from .models import Client, Fruit, Currency, Basket

#register classes for the admin to see it on 127.0.0.1:8000
admin.site.register(Client)
admin.site.register(Fruit)
admin.site.register(Currency)
admin.site.register(Basket)