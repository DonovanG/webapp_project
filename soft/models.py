from django.db import models

#Clients that buy some fruits monthly to the farmer
class Client(models.Model):
    #attributes
    name = models.CharField(max_length=120)
    country = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    gender = models.CharField(max_length=10)
    fruit_currency_preference_1 = models.CharField(max_length=120)
    fruit_currency_preference_2 = models.CharField(max_length=120)
    fruit_currency_preference_3 = models.CharField(max_length=120)
    fruit_size_preference_1 = models.CharField(max_length=120)
    fruit_size_preference_2 = models.CharField(max_length=120)
    fruit_size_preference_3 = models.CharField(max_length=120)
    fruit_type_preference_1 = models.CharField(max_length=120)
    fruit_type_preference_2 = models.CharField(max_length=120)

    #display the name by default
    def __str__(self):
        return self.name

#Fruits that the customers can buy monthly, the user can filter the fruits and see some statistics
class Fruit(models.Model):
    
    #attributes
    name = models.CharField(max_length=120)
    prohibited_country = models.CharField(max_length=50)
    currency = models.CharField(max_length=50)
    size = models.CharField(max_length=10)
    metric = models.CharField(max_length=120)
    price_unit = models.IntegerField(default=1)
    price_month1 = models.CharField(max_length=10)
    price_month2 = models.CharField(max_length=10)
    price_month3 = models.CharField(max_length=10)
    price_month4 = models.CharField(max_length=10)
    price_month5 = models.CharField(max_length=10)
    price_month6 = models.CharField(max_length=10)
    price_month7 = models.CharField(max_length=10)
    price_month8 = models.CharField(max_length=10)
    price_month9 = models.CharField(max_length=10)
    price_month10 = models.CharField(max_length=10)
    price_month11 = models.CharField(max_length=10)
    price_month12 = models.CharField(max_length=10)
    volume_month1 = models.IntegerField(default=1)
    volume_month2 = models.IntegerField(default=1)
    volume_month3 = models.IntegerField(default=1)
    volume_month4 = models.IntegerField(default=1)
    volume_month5 = models.IntegerField(default=1)
    volume_month6 = models.IntegerField(default=1)
    volume_month7 = models.IntegerField(default=1)
    volume_month8 = models.IntegerField(default=1)
    volume_month9 = models.IntegerField(default=1)
    volume_month10 = models.IntegerField(default=1)
    volume_month11 = models.IntegerField(default=1)
    volume_month12 = models.IntegerField(default=1)
    img_path = models.CharField(max_length=120)

    #display the name by default
    def __str__(self):
        return self.name

#Allow to see different currencies evolutions
class Currency(models.Model):
    #attributes
    currency = models.CharField(max_length=120)
    euro_month_1 = models.CharField(max_length=10)
    euro_month_2 = models.CharField(max_length=10)
    euro_month_3 = models.CharField(max_length=10)
    euro_month_4 = models.CharField(max_length=10)
    euro_month_5 = models.CharField(max_length=10)
    euro_month_6 = models.CharField(max_length=10)
    euro_month_7 = models.CharField(max_length=10)
    euro_month_8 = models.CharField(max_length=10)
    euro_month_9 = models.CharField(max_length=10)
    euro_month_10 = models.CharField(max_length=10)
    euro_month_11 = models.CharField(max_length=10)
    euro_month_12 = models.CharField(max_length=10)
    sterling_month_1 = models.CharField(max_length=10)
    sterling_month_2 = models.CharField(max_length=10)
    sterling_month_3 = models.CharField(max_length=10)
    sterling_month_4 = models.CharField(max_length=10)
    sterling_month_5 = models.CharField(max_length=10)
    sterling_month_6 = models.CharField(max_length=10)
    sterling_month_7 = models.CharField(max_length=10)
    sterling_month_8 = models.CharField(max_length=10)
    sterling_month_9 = models.CharField(max_length=10)
    sterling_month_10 = models.CharField(max_length=10)
    sterling_month_11 = models.CharField(max_length=10)
    sterling_month_12 = models.CharField(max_length=10)
    dollard_month_1 = models.CharField(max_length=10)
    dollard_month_2 = models.CharField(max_length=10)
    dollard_month_3 = models.CharField(max_length=10)
    dollard_month_4 = models.CharField(max_length=10)
    dollard_month_5 = models.CharField(max_length=10)
    dollard_month_6 = models.CharField(max_length=10)
    dollard_month_7 = models.CharField(max_length=10)
    dollard_month_8 = models.CharField(max_length=10)
    dollard_month_9 = models.CharField(max_length=10)
    dollard_month_10 = models.CharField(max_length=10)
    dollard_month_11 = models.CharField(max_length=10)
    dollard_month_12 = models.CharField(max_length=10)

    #display the currency name by default
    def __str__(self):
        return self.currency

#A basket table, for each raw a client is link to a fruit for a specific month
class Basket(models.Model):
    #attributes
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE) #the client who buy it
    id_fruit = models.ForeignKey(Fruit, on_delete=models.CASCADE) #thee fruit that the client add to the basket
    volume = models.IntegerField() #the number of this fruit that he bought
    month = models.IntegerField() #the month when he bought it

    #display the basket's caracteristics by default
    def __str__(self):
        return "client : "+self.id_client.name+" fruit : "+self.id_fruit.name+" volume : "+str(self.volume)+" month : "+str(self.month)


    