from rest_framework import serializers

from soft.models import Client, Fruit, Currency, Basket

#serialize a client for CRUD updates in react
class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('id','name', 'country', 'city', 'gender', 'fruit_currency_preference_1', 'fruit_currency_preference_2', 'fruit_currency_preference_3', 'fruit_size_preference_1', 'fruit_size_preference_2', 'fruit_size_preference_3', 'fruit_type_preference_1', 'fruit_type_preference_2')

#serialize a fruit for CRUD updates in react
class FruitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fruit
        fields = ('id','name', 'prohibited_country', 'currency', 'size', 'metric', 'price_unit',
         'price_month1', 'price_month2', 'price_month3', 'price_month4', 'price_month5', 'price_month6', 'price_month7', 'price_month8', 'price_month9', 'price_month10', 'price_month11', 'price_month12'
         , 'volume_month1', 'volume_month2', 'volume_month3', 'volume_month4', 'volume_month5', 'volume_month6', 'volume_month7', 'volume_month8', 'volume_month9', 'volume_month10', 'volume_month11', 'volume_month12',
         'img_path'
         )
#serialize a currency for CRUD updates in react
class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ('id','currency', 'euro_month_1', 'euro_month_2', 'euro_month_3', 'euro_month_4', 'euro_month_5', 'euro_month_6', 'euro_month_7', 'euro_month_8', 'euro_month_9', 'euro_month_10', 'euro_month_11', 'euro_month_12',
        'sterling_month_1',  'sterling_month_2',  'sterling_month_3',  'sterling_month_4',  'sterling_month_5',  'sterling_month_6',  'sterling_month_7',  'sterling_month_8',  'sterling_month_9',  'sterling_month_10',  'sterling_month_11',  'sterling_month_12'
        , 'dollard_month_1', 'dollard_month_2', 'dollard_month_3', 'dollard_month_4', 'dollard_month_5', 'dollard_month_6', 'dollard_month_7', 'dollard_month_8', 'dollard_month_9', 'dollard_month_10', 'dollard_month_12', 'dollard_month_12')

#serialize a basket for CRUD updates in react
class BasketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Basket
        fields = ('id_client','id_fruit', 'volume', 'month')
