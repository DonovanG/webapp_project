import django_filters.rest_framework
from rest_framework import viewsets, generics
from soft.models import Client, Fruit, Currency, Basket
from .serializer import ClientSerializer,FruitSerializer, CurrencySerializer, BasketSerializer

#view for diasplaying and filtering clients
class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    filter_fields = ('country',)

#view for diasplaying fruits
class FruitViewSet(viewsets.ModelViewSet):
    serializer_class = FruitSerializer
    queryset = Fruit.objects.all()

#view for diasplaying and filtering currencies
class CurrencyViewSet(viewsets.ModelViewSet):
    serializer_class = CurrencySerializer
    queryset = Currency.objects.all()
    filter_fields = ('currency',)
 

#view for diasplaying baskets
class BasketViewSet(viewsets.ModelViewSet):
    serializer_class = BasketSerializer
    queryset = Basket.objects.all()
    filter_fields = ('id_client',)
