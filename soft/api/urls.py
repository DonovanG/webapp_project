from soft.api.view import ClientViewSet, FruitViewSet, CurrencyViewSet, BasketViewSet
from rest_framework.routers import DefaultRouter

#some routes for the react app
router = DefaultRouter()
router.register(r'client', ClientViewSet, basename='client')
router.register(r'fruit', FruitViewSet, basename='fruit')
router.register(r'currency', CurrencyViewSet, basename='currency')
router.register(r'basket', BasketViewSet, basename='basket')

urlpatterns = router.urls


