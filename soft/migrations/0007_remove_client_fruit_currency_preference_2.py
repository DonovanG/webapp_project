# Generated by Django 2.1.4 on 2019-01-08 13:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('soft', '0006_auto_20190107_0931'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='fruit_currency_preference_2',
        ),
    ]
